<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Agenda Telefonica</title>
    <?php
       include("../css/bootstrap.php");
     ?>

</head>

<body>

<?php
  include("../view/menu.php");
?>


<div class="panel panel-warning">
  <div class="panel-heading">
  <h3 class="panel-title"> Formulario de cadastro</h3>
 
    </div>

<div class="panel-body">
  
 <forms action="">
  <div class="forms-group col-md-12 col-xs-12 col-sm-12">
    <label for="nome">Nome</label><span class"text-red">*</span>
    <input type="text" name="nome" id="nome" required class="form-control">
  </div>

  <div class="forms-group col-md-6 col-xs-6 col-sm-6">
    <label for="foneres">Fone Residencial</label><span class"text-red">*</span>
    <input type="text" name="telefone" id="telefone" required class="form-control">
  </div>

   <div class="forms-group col-md-6 col-xs-6 col-sm-6">
     <label for="fonecel">Fone Celular</label><span class"text-red">*</span>
     <input type="text" name="celular" id="celular" required class="form-control">
  </div>

  <div class="forms-group">
    <label for="data">Data</label><span class"text-read">*</span>
    <input  type="text" name="data" id="daya" require class="form-control">
  </div>

  <div class="forms-group">
    <label for="endereco">Endereço</label><span class"text-read">*</span>
    <input  type="text" name="endereco" id="enndereco" require class="form-control">
  </div>

  

<div class="forms-group col-md-12 col-xs-12 col-sm-12">
   <label for="email">Email</label><span class"text-red">*</span>
   <input type="text" name="email" id="email" required class="form-control">
</div>

<div class="forms-group col-md-3 col-xs-3 col-sm-3">
   <label for="dtnase">Data Nacimento</label><span class"text-red">*</span>
   <input type="text" name="nacimento" id="nacimento" required class="form-control">
</div>

  <div class="forms-group  col-md-12 col-xs-12">
    <label for="genero"> Genero</label>
    <select name="genero" id="genero" class "forms-control">
      <option value="M"> Masculino </option>
      <option value="F"> Feminino </option>
      <option value="O"> Outros </option>
    </select>
  </div>

</forms>
</div>

<div class="panel-footer">

<div class ="col-md-12">
   <button  class="btn btn-default btn-lg"> <i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Save</button>
   <button type="submit" class="btn btn-success btn-lg "> <i class="fa fa-floppy-o" aria-hidden="true"></i> Gravar</button>
  </div>
</div>

</div>   




<!--  Aqui podemos colocar o codigo HTML -->
<?php
  include("../js/bootstrap.php");
?>
</body>
</html>